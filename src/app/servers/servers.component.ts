import { Component } from '@angular/core';

@Component({
  selector: 'app-servers',
  // template: '<app-server></app-server><app-server></app-server>',
  templateUrl: './servers.component.html',
  styleUrl: './servers.component.css'
})
export class ServersComponent {
  allowNewServer = false;
  serverCreationStatus = 'No server was created';
  serverName = 'test';
  userName = '';
  serverCreated = false;
  servers = ['test-server-1', 'test-server-2'];
  textStatus = false;
  clickLogs: (string | number)[] = [];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  onCreateServer() {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created. Name is ' + this.serverName;
  }

  onToggleText() {
    this.textStatus = !this.textStatus;
    this.clickLogs.push(this.clickLogs.length + 1);
    console.log(this.clickLogs);
    console.log(this.clickLogs.values);
  }

  resetUserName() {
    this.userName = '';
  }

  onUpdateServerName(event: Event) {
    console.log(event);
    this.serverName = (<HTMLInputElement>event.target).value;
  }
}
